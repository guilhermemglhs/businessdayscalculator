import sys							#arguments
from datetime import datetime	#dates
from datetime import time 		#dates
from datetime import timedelta	#deltadates

#start and end times of an office day
officeHour = [time(hour=8), time(hour=12), time(hour=14), time(hour=18)]
#total business time  per day
businessHourPerDay = officeHour[1].hour - officeHour[0].hour + officeHour[3].hour - officeHour[2].hour

daysOfTheWeek = 7
businessDayOfTheWeek = 5
secondsInOneHour=3600

def getBusinessDaysBetween(startDate, endDate):
	#if start day and end day are the same, zero days between them
	if(startDate.day == endDate.day and startDate.month == endDate.month and startDate.year == endDate.year):
		return 0

	#calculate number of days (excluding startDay and endDay)
	nDays = abs((endDate - startDate).days) - 1

	#calculate number of weeks and extra days
	nWeeks = nDays/daysOfTheWeek
	extraDays = nDays%daysOfTheWeek

	#if start week day is greater than end week day, subtract weekend from extra days
	if(startDate.weekday() > endDate.weekday()):
		extraDays = extraDays - 2

	if extraDays < 0:
		extraDays = 0

	return nWeeks*businessDayOfTheWeek + extraDays

def getBusinessHoursBetween(startDate, endDate):
	#get number of complete days between dates (excluding first and last days)
	completeDays = getBusinessDaysBetween(startDate, endDate)

	startTime = time(hour=startDate.hour, minute=startDate.minute)
	endTime = time(hour=endDate.hour, minute=endDate.minute)

	extraHours = 0

	#verifies hours worked in first day
	if startTime.hour <= officeHour[1].hour:
		if startTime.hour <= officeHour[0].hour:
			extraHours = extraHours + (officeHour[1].hour - officeHour[0].hour) + (officeHour[3].hour - officeHour[2].hour)
		else:
			extraHours = extraHours + (officeHour[1].hour - startTime.hour) + (officeHour[3].hour - officeHour[2].hour)
	elif startTime.hour <= officeHour[3].hour:
		if startTime.hour <= officeHour[2].hour:
			extraHours = extraHours + (officeHour[3].hour - officeHour[2].hour)
		else:
			extraHours = extraHours + (officeHour[3].hour - startTime.hour)

	print extraHours

	#verifies hours worked in last day
	if endTime.hour > officeHour[0].hour:
		if endTime.hour <= officeHour[1].hour :
			extraHours = extraHours + (endTime.hour - officeHour[0].hour)
		elif endTime.hour < officeHour[3].hour :
			extraHours = extraHours + (endTime.hour - officeHour[2].hour) +  officeHour[1].hour - officeHour[0].hour
		elif endTime.hour >= officeHour[3].hour :
			extraHours = extraHours +   officeHour[1].hour - officeHour[0].hour +   officeHour[3].hour - officeHour[2].hour

	# print completeDays

	return timedelta(hours=completeDays * businessHourPerDay + extraHours)

#create datetime object with arguments list
def getDate(date):
	year = int(date[0])
	month = int(date[1])
	day = int(date[2])
	hour = int(date[3])
	minute = int(date[4])
	return datetime(year, month, day, hour, minute)

if __name__ == "__main__":
	#remove file name from arguments
	argv = sys.argv[1:]
	#divide arguments list in two
	startDateArgs = argv[:len(argv)/2]
	endDateArgs = argv[len(argv)/2:]

	#get datetime  objects
	startDate = getDate(startDateArgs);
	endDate = getDate(endDateArgs);

	#verifies data consistence
	if endDate < startDate:
		print "End date must be greater than start date"
		exit(0)

	#calculate number of hours
	delta = getBusinessHoursBetween(startDate, endDate)

	#using thsi "delta.total_seconds()/secondsInOneHour" because deltatime object don't have hours attribute, only seconds and days (!)
	deltaHours = delta.total_seconds()/secondsInOneHour

	if delta.days > 1:
		print "Business hours between dates: " + str(delta) + ", or " + str(deltaHours) + " hours. "
	else:
		print "Business hours between dates: " + str(deltaHours) + " hours. "