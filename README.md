# README #

Quick application for calculating business hours between two dates.

### Configurations ###

This application was developed using Python 2.7, so you'll need to install the appropriate interpreter.

In line 7 you can configure the office hours:
Default: 8h-12h / 14h-18h

In line 12 may be configured the number of business day of the week. 
Default: 5 (mon-fri)

### Running Tests ###

python dates.py yyyy mm dd hh mm yyyy mm dd hh mm

Passing start date and end date by parameters where
yyyy = year
mm = month
dd = day
hh = hour
mm = minutes

### Output ###

Should be something like:
Business hours between dates: XX days, YY:YY, or ZZ hours. ZZ being the total number of hours, i.e., XX*aa+YY:YY where aa is the total number of hours in one working day (8 hours by default).

### Developer ###

* Guilherme Magalhães ggmagalhaes@inf.ufrgs.br